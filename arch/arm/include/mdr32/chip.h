/************************************************************************************
 * arch/arm/include/mdr32/chip.h
 *
 *   Copyright (C) 2009, 2011-2014, 2017-2018 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __ARCH_ARM_INCLUDE_MDR32_CHIP_H
#define __ARCH_ARM_INCLUDE_MDR32_CHIP_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Check the MDR32 family configuration.
 * It must be done in arch/arm/src/mdr32/Kconfig !
 */

#ifdef CONFIG_MDR32_MDR32F9Q2I
#  define __HAVE_F9Q2I  1
#else
#  define __HAVE_F9Q2I  0
#endif
#ifdef CONFIG_MDR32_MDR32VE1QI
#  define __HAVE_VE1QI  1
#else
#  define __HAVE_VE1QI  0
#endif

#if ((__HAVE_F9Q2I + __HAVE_VE1QI ) != 1)
#  error "Only one MDR32 family must be selected !"
#endif


/* Get customizations for each supported chip and provide alternate function pin-mapping
 *
 * NOTE: Each GPIO pin may serve either for general purpose I/O or for a special
 * alternate function (such as USART, CAN, USB, SDIO, etc.).  That particular
 * pin-mapping will depend on the package and MDR32 family.  If you are incorporating
 * a new MDR32 chip into NuttX, you will need to add the pin-mapping to a header file
 * and to include that header file below. The chip-specific pin-mapping is defined in
 * the chip datasheet.
 */

/* MDR32L Line ************************************************************/

/*
 * MDR32F9Q2I -- 64-pins
 * MDR32VE1QI -- 144-pins
 *
 * MDR32F9Q2I -- 128KB FLASH, 32KB SRAM, 4KB EEPROM
 * MDR32VE1QI -- 128KB FLASH, 32KB SRAM, 4KB EEPROM
 *
 */

#if defined(CONFIG_ARCH_CHIP_MDR32F9Q2I)
#  define MDR32_NFSMC                    1   /* No FSMC */
#  define MDR32_NATIM                    0   /* No advanced timers */
#  define MDR32_NGTIM                    3   /* 16-bit general up/down timers TIM2-4 with DMA */
#  define MDR32_NGTIMNDMA                3   /* 16-bit general timers TIM9-11 without DMA */
#  define MDR32_NBTIM                    2   /* 2 basic timers: TIM6, TIM7 with DMA */
#  define MDR32_NDMA                     1   /* DMA1, 7-channels */
#  define MDR32_NSPI                     2   /* SPI1-2 */
#  define MDR32_NUSART                   2   /* USART1-3 */
#  define MDR32_NI2C                     1   /* I2C1-2 */
#  define MDR32_NCAN                     2   /* No CAN */
#  define MDR32_NSDIO                    0   /* No SDIO */
#  define MDR32_NUSBOTG                  1   /* No USB OTG FS/HS (only USB 2.0 device) */
#  define MDR32_NGPIO                    37  /* GPIOA-E,H */
#  define MDR32_NADC                     1   /* ADC1, 14-channels */
#  define MDR32_NDAC                     2   /* DAC 1, 2 channels */
#  define MDR32_NCMP                     2   /* (2) Comparators */
#  define MDR32_NCAPSENSE                13  /* Capacitive sensing channels */
#  define MDR32_NCRC                     0   /* No CRC */
#  define MDR32_NETHERNET                0   /* No Ethernet */
#  define MDR32_NRNG                     0   /* No random number generator (RNG) */
#  define MDR32_NARINC429_RX             0   /* No ARINC429_RX */
#  define MDR32_NARINC429_TX             0   /* No ARINC429_TX */
#  define MDR32_NMILSTD1553	             0   /* No MIL-STD-1553 */

#elif defined(CONFIG_ARCH_CHIP_MDR32VE1QI)
#  define MDR32_NFSMC                    1   /* No FSMC */
#  define MDR32_NATIM                    0   /* No advanced timers */
#  define MDR32_NGTIM                    3   /* 16-bit general up/down timers TIM2-4 with DMA */
#  define MDR32_NGTIMNDMA                3   /* 16-bit general timers TIM9-11 without DMA */
#  define MDR32_NBTIM                    2   /* 2 basic timers: TIM6, TIM7 with DMA */
#  define MDR32_NDMA                     1   /* DMA1, 7-channels */
#  define MDR32_NSPI                     2   /* SPI1-2 */
#  define MDR32_NUSART                   2   /* USART1-3 */
#  define MDR32_NI2C                     1   /* I2C1-2 */
#  define MDR32_NCAN                     2   /* No CAN */
#  define MDR32_NSDIO                    0   /* No SDIO */
#  define MDR32_NUSBOTG                  1   /* No USB OTG FS/HS (only USB 2.0 device) */
#  define MDR32_NGPIO                    51  /* GPIOA-E,H */
#  define MDR32_NADC                     1   /* ADC1, 20-channels */
#  define MDR32_NDAC                     2   /* DAC , 2 channels */
#  define MDR32_NCMP                     2   /* (2) Comparators */
#  define MDR32_NCAPSENSE                20  /* Capacitive sensing channels */
#  define MDR32_NCRC                     0   /* No CRC */
#  define MDR32_NETHERNET                1   /* No Ethernet */
#  define MDR32_NRNG                     0   /* No random number generator (RNG) */
#  define MDR32_NARINC429_RX             8   /* No ARINC429_RX */
#  define MDR32_NARINC429_TX             4   /* No ARINC429_TX */
#  define MDR32_NMILSTD1553	             2   /* No MIL-STD-1553 */


#else
#  error "Unsupported MDR32 chip"
#endif

/* NVIC priority levels *************************************************************/

#define NVIC_SYSH_PRIORITY_MIN     0xf0 /* All bits set in minimum priority */
#define NVIC_SYSH_PRIORITY_DEFAULT 0x80 /* Midpoint is the default */
#define NVIC_SYSH_PRIORITY_MAX     0x00 /* Zero is maximum priority */
#define NVIC_SYSH_PRIORITY_STEP    0x10 /* Four bits of interrupt priority used */

#endif /* __ARCH_ARM_INCLUDE_MDR32_CHIP_H */
